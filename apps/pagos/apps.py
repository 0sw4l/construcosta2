from django.apps import AppConfig


class PagosConfig(AppConfig):
    name = 'apps.pagos'

    def ready(self):
        from . import signals
        print('ready pagos')
