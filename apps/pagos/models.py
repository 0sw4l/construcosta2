from django.db import models

# Create your models here.


class CategoriaTarjeta(models.Model):
    nombre = models.CharField(max_length=50)
    logo = models.URLField()


class TarjetaCredito(models.Model):
    numero = models.PositiveIntegerField(unique=True)
    ccv = models.PositiveSmallIntegerField()
    cupo = models.PositiveIntegerField()


class TarjetaEmpresa(models.Model):
    expiracion = models.CharField(max_length=50)
    tarjeta_credito = models.ForeignKey(TarjetaCredito)
    cliente = models.ForeignKey('clientes.Cliente')


class TransaccionTarjeta(models.Model):
    hora = models.TimeField(auto_now_add=True)
    fecha = models.DateField(auto_now_add=True)
    valor = models.PositiveIntegerField()
    tarjeta_credito = models.ForeignKey(TarjetaCredito)
    cliente = models.ForeignKey('clientes.Cliente')

