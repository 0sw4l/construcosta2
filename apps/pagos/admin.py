from django.contrib import admin
from apps.pagos.models import CategoriaTarjeta, TarjetaCredito, TarjetaEmpresa, TransaccionTarjeta


@admin.register(CategoriaTarjeta)
class CategoriaTarjetaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']


@admin.register(TarjetaCredito)
class TarjetaCreditoAdmin(admin.ModelAdmin):
    list_display = ['numero', 'ccv', 'cupo']


@admin.register(TarjetaEmpresa)
class TarjetaEmpresaAdmin(admin.ModelAdmin):
    list_display = ['expiracion', 'tarjeta_credito', 'cliente']


@admin.register(TransaccionTarjeta)
class TransaccionTarjetaAdmin(admin.ModelAdmin):
    list_display = ['hora', 'fecha', 'valor', 'tarjeta_credito', 'cliente']
