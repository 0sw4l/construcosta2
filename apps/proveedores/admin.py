from django.contrib import admin

# Register your models here.
from apps.proveedores.forms import ProveedorForm
from apps.proveedores.models import Proveedor, CategoriaProducto, ProductoProveedor, Marca


@admin.register(Proveedor)
class ProveedorAdmin(admin.ModelAdmin):
    list_display = ['get_full_name', 'nit', 'rut']
    form = ProveedorForm


@admin.register(CategoriaProducto)
class CategoriaProductoAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre', 'productos']


@admin.register(ProductoProveedor)
class ProductoProveedorAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'categoria_producto', 'valor', 'proveedor']


@admin.register(Marca)
class MarcaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre', 'productos']
