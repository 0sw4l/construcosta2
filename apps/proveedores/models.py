from django.contrib.auth.models import User
from django.db import models
from django.core import urlresolvers
from django.contrib.humanize.templatetags.humanize import intcomma
# Create your models here.
from apps.servicios.models import BaseNombre
from apps.ventas.models import PedidoCliente


class Proveedor(User):
    nit = models.CharField(max_length=50, unique=True, blank=True, null=True)
    rut = models.CharField(max_length=50, unique=True, blank=True, null=True)
    imagen = models.URLField(blank=True, null=True)
    direccion = models.CharField(max_length=50)
    telefono = models.PositiveIntegerField()
    TIPOS = (
        ('EMPRESA', 'EMPRESA'),
        ('PERSONA INDEPENDIENTE', 'PERSONA INDEPENDIENTE')
    )
    tipo = models.CharField(max_length=50, choices=TIPOS)

    class Meta:
        verbose_name = 'Usuario Proveedor'
        verbose_name_plural = 'Usuarios Proveedores'

    def __str__(self):
        return self.get_full_name().upper()

    def get_absolute_url(self):
        return urlresolvers.reverse_lazy('proveedores:proveedor',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def get_productos(self):
        return ProductoProveedor.objects.filter(proveedor=self)

    def get_historial_pedidos(self):
        return PedidoCliente.objects.filter(proveedor=self)

    def get_pedidos_enviados(self):
        return PedidoCliente.objects.filter(proveedor=self, enviado=True)

    def get_pedidos_recibidos(self):
        return PedidoCliente.objects.filter(proveedor=self, recibido=True)


class CategoriaProducto(BaseNombre):
    def productos(self):
        return ProductoProveedor.objects.filter(categoria_producto=self).count()


class Marca(BaseNombre):
    def productos(self):
        return ProductoProveedor.objects.filter(marca=self).count()


class ProductoProveedor(models.Model):
    nombre = models.CharField(max_length=50)
    categoria_producto = models.ForeignKey(CategoriaProducto)
    marca = models.ForeignKey(Marca)
    valor = models.PositiveIntegerField()
    proveedor = models.ForeignKey(Proveedor)
    imagen = models.URLField(default='https://http2.mlstatic.com/S_912711-MLA20615911030_032016-O.jpg')

    def __str__(self):
        return self.nombre

    def get_update_url(self):
        return urlresolvers.reverse_lazy('proveedores:modificar_producto',
                                         kwargs={
                                             'pk':self.pk
                                         })
