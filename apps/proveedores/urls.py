from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^crear-producto/',
        views.ProductoProveedorCreateView.as_view(),
        name='crear_producto'),

    url(r'^modificar-producto/(?P<pk>\d+)/',
        views.ProductoProveedorUpdateView.as_view(),
        name='modificar_producto'),

    url(r'^lista-productos/',
        views.ProductoProveedorListView.as_view(),
        name='lista_productos'),

    url(r'^productos/',
        views.ProductoListView.as_view(),
        name='lista_productos_all'),

    url(r'^proveedor/(?P<pk>\d+)/',
        views.ProveedorDetailView.as_view(),
        name='proveedor'),

    url(r'^proveedores/',
        views.ProveedorListView.as_view(),
        name='proveedores'),
]