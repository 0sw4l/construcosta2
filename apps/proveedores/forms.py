from apps.proveedores.models import Marca, CategoriaProducto
from apps.utils import forms
from . import models


class ProveedorForm(forms.BaseUserCreationForm):
    title = 'Proveedor'

    class Meta(forms.BaseUserCreationForm.Meta):
        model = models.Proveedor


class ProductoProveedorForm(forms.ProveedorObjectMixinForm,
                            forms.FormAllFields):

    title = 'Producto'

    class Meta(forms.FormAllFields.Meta):
        model = models.ProductoProveedor


class FiltroProductoForm(forms.forms.Form):
    nombre = forms.forms.CharField(max_length=200)
    marca = forms.forms.ModelChoiceField(
        queryset=Marca.objects.all()
    )
    categoria = forms.forms.ModelChoiceField(
        queryset=CategoriaProducto.objects.all()
    )
