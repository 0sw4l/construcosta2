from django.apps import AppConfig


class ProveedoresConfig(AppConfig):
    name = 'apps.proveedores'

    def ready(self):
        from . import signals
        print('ready proveedores')
