from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.detail import BaseDetailView, DetailView

from apps.proveedores.forms import FiltroProductoForm
from apps.proveedores.models import ProductoProveedor, Proveedor
from django.core import urlresolvers
from . import models
from apps.utils.views import SetProveedorFormView, BaseCreateView, FilterProveedorUser, BaseListViewDinamicHeader, \
    BaseUpdateView, BaseListView
from . import forms
# Create your views here.


class ProveedorCreateViewMixin(SetProveedorFormView):
    form_class = forms.ProductoProveedorForm
    success_url = urlresolvers.reverse_lazy('proveedores:lista_productos')


class ProductoProveedorCreateView(ProveedorCreateViewMixin, BaseCreateView):
    pass


class ProductoProveedorUpdateView(ProveedorCreateViewMixin, BaseUpdateView):
    model = models.ProductoProveedor


class ProductoProveedorListView(FilterProveedorUser):
    model = ProductoProveedor
    HEADER = ('nombre', 'categoria', 'marca', 'valor')
    template_name = 'apps/proveedores/lista_productos.html'


class BaseReturnFrontendFilterForm(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = FiltroProductoForm()
        return context


class ProductoListView(BaseReturnFrontendFilterForm, BaseListView):
    model = ProductoProveedor
    template_name = 'apps/proveedores/productos_all.html'


class ProveedorDetailView(LoginRequiredMixin, BaseReturnFrontendFilterForm, DetailView):
    model = Proveedor
    template_name = 'apps/proveedores/detalle_proveedor.html'


class ProveedorListView(LoginRequiredMixin, ListView):
    model = Proveedor
    template_name = 'apps/proveedores/lista_proveedores.html'

