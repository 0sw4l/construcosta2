from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse_lazy


class BaseNombre(models.Model):
    nombre = models.CharField(max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre


class CategoriaProfesion(BaseNombre):
    def personas(self):
        return Persona.objects.filter(profesion=self).count()


class CategoriaServicio(BaseNombre):
    def personas(self):
        return Persona.objects.filter(servicio=self).count()


class Persona(User):
    ESTADOS_CIVILES = (
        ('soltero/a', 'soltero/a'),
        ('casado/a', 'casado/a'),
        ('divorciado/a', 'divorciado/a'),
        ('viudo/a', 'viudo/a')
    )
    estado_civil = models.CharField(max_length=20, choices=ESTADOS_CIVILES)
    foto = models.URLField(default='http://www.alltok.com.br/wp-content/uploads/sample/2014/07/people-img8.jpg')
    servicio = models.ForeignKey(CategoriaServicio)
    profesion = models.ForeignKey(CategoriaProfesion)
    telefono = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    SEXO = (
        ('masculino', 'masculino'),
        ('femenino', 'femenino'),
        ('otro', 'otro')
    )
    sexo = models.CharField(max_length=20, choices=SEXO)
    NIVEL_ACADEMICO = (
        ('basica primaria', 'basica primaria'),
        ('basica secundaria', 'basica secundaria'),
        ('tecnico', 'tecnico'),
        ('tecnologo', 'tecnologo'),
        ('profesional', 'profesional')
    )
    nivel_academico = models.CharField(max_length=30, choices=NIVEL_ACADEMICO)
    estudios_adicionales = models.TextField()
    experiencia_laboral = models.PositiveIntegerField()
    nombre_empresa_anterior = models.CharField(max_length=200)
    cargo_empresa_anterior = models.CharField(max_length=200)
    periodo_inicial_empresa_anterior = models.DateField()
    periodo_final_empresa_anterior = models.DateField()
    funciones_empresa_anterior = models.TextField()
    valor_hora = models.PositiveIntegerField()
    calificacion = models.DecimalField(max_digits=3, decimal_places=2, default=5.00)

    def get_absolute_url(self):
        return reverse_lazy('servicios:detalle_persona', kwargs={'pk': self.pk})



