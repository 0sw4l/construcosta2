from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^personas/',
        views.PersonaListView.as_view(),
        name='lista_personas'),

    url(r'^persona/(?P<pk>\d+)/',
        views.PersonaDetailView.as_view(),
        name='detalle_persona')
]
