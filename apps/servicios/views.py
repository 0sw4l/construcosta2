from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.views.generic import DetailView
from django.views.generic import ListView

from apps.servicios.forms import FiltroPersonaForm
from apps.servicios.models import Persona


class BaseReturnFrontendFilterForm(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = FiltroPersonaForm()
        return context


class PersonaListView(LoginRequiredMixin, ListView):
    model = Persona
    template_name = 'apps/servicios/lista_personas.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = FiltroPersonaForm()
        return context


class PersonaDetailView(LoginRequiredMixin, DetailView):
    model = Persona
    template_name = 'apps/servicios/detalle_persona.html'
