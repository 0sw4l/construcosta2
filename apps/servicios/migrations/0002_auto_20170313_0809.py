# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-13 13:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='foto',
            field=models.URLField(default='https://cdn.pixabay.com/photo/2012/04/26/19/47/man-42934_960_720.png'),
        ),
    ]
