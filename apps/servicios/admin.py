from django.contrib import admin

from apps.servicios.forms import PersonaForm
from .models import CategoriaProfesion, CategoriaServicio, Persona
# Register your models here.


@admin.register(CategoriaProfesion)
class CategoriaProfesionAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'personas']


@admin.register(CategoriaServicio)
class CategoriaServicioAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'personas']


@admin.register(Persona)
class PersonaAdmin(admin.ModelAdmin):
    list_display = ['get_full_name', 'servicio', 'profesion', 'sexo', 'nivel_academico', 'valor_hora']
    form = PersonaForm


