from django import  forms

from apps.servicios.models import CategoriaProfesion, CategoriaServicio
from apps.utils.forms import BaseUserCreationForm
from . import models


class PersonaForm(BaseUserCreationForm):
    title = 'Persona'

    class Meta(BaseUserCreationForm.Meta):
        model = models.Persona


class FiltroPersonaForm(forms.Form):
    profesion = forms.ModelChoiceField(
        queryset=CategoriaProfesion.objects.all()
    )
    servicio = forms.ModelChoiceField(
        queryset=CategoriaServicio.objects.all()
    )
