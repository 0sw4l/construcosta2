from django.conf.urls import url, include
from rest_framework import routers
from . import views, viewsets

router = routers.DefaultRouter()
router.register(r'categorias', viewsets.CategoriaProductoViewSet)
router.register(r'marcas', viewsets.MarcaViewSet)
router.register(r'productos-proveedor', viewsets.ProductoProveedorViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^productos/', views.ProductoProveedortList.as_view())
]
