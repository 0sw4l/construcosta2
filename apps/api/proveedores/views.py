from rest_framework import generics
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.api.proveedores.serializers import ProductoProveedorSerializer
from apps.proveedores.models import ProductoProveedor


class BaseApiViewList(mixins.ListModelMixin, generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ProductoProveedortList(BaseApiViewList):
    queryset = ProductoProveedor.objects.all()
    serializer_class = ProductoProveedorSerializer

