from rest_framework import serializers
from apps.proveedores import models


class ProductoProveedorSerializer(serializers.ModelSerializer):
    categoria_nombre = serializers.CharField(
        source='categoria_producto.nombre',
        read_only=True
    )

    marca_nombre = serializers.CharField(
        source='marca.nombre',
        read_only=True
    )

    proveedor_nombre = serializers.CharField(
        source='proveedor.first_name',
        read_only=True
    )

    proveedor_url = serializers.CharField(
        source='proveedor.get_absolute_url',
        read_only=True
    )

    class Meta:
        model = models.ProductoProveedor
        fields = ('id', 'nombre', 'imagen', 'categoria_producto', 'categoria_nombre', 'marca',
                  'marca_nombre', 'valor', 'proveedor', 'proveedor_nombre', 'proveedor_url')


class BaseSerializerName(serializers.ModelSerializer):
    class Meta:
        model = None
        fields = ('id', 'nombre', 'productos')


class CategoriaProductoSerializer(BaseSerializerName):
    class Meta(BaseSerializerName.Meta):
        model = models.CategoriaProducto


class MarcaSerializer(BaseSerializerName):
    class Meta(BaseSerializerName.Meta):
        model = models.Marca