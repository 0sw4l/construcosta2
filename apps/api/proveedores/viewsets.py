from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.api.proveedores.serializers import CategoriaProductoSerializer, ProductoProveedorSerializer, MarcaSerializer
from apps.proveedores.models import CategoriaProducto, ProductoProveedor, Marca


class BaseNameViewSet(viewsets.ReadOnlyModelViewSet):
    attr = ''
    model = None
    permission_classes = (IsAuthenticated,)

    @detail_route(methods=['get'])
    def productos(self, request, pk=None):
        productos = ProductoProveedor.objects.filter(**{self.attr: pk})
        serializer = ProductoProveedorSerializer(productos, many=True)
        return Response(serializer.data)


class CategoriaProductoViewSet(BaseNameViewSet):
    queryset = CategoriaProducto.objects.all()
    serializer_class = CategoriaProductoSerializer
    attr = 'categoria_producto_id'


class MarcaViewSet(BaseNameViewSet):
    queryset = Marca.objects.all()
    serializer_class = MarcaSerializer
    attr = 'marca_id'


class ProductoProveedorViewSet(BaseNameViewSet):
    queryset = ProductoProveedor.objects.all()
    serializer_class = ProductoProveedorSerializer
    attr = 'proveedor_id'

