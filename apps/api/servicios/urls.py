from django.conf.urls import url, include
from rest_framework import routers
from . import views, viewsets

router = routers.DefaultRouter()
router.register(r'categorias-profesion', viewsets.CategoriaProfesionViewSet),
router.register(r'categorias-servicios', viewsets.CategoriaServicioViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^personas/', views.PersonaApiView.as_view())
]
