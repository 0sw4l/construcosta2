from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.api.servicios.serializers import PersonaSerializer, CategoriaProfesionSerializer, CategoriaServicioSerializer
from apps.servicios.models import Persona, CategoriaProfesion, CategoriaServicio


class BaseNameViewSet(viewsets.ReadOnlyModelViewSet):
    attr = ''
    model = None
    permission_classes = (IsAuthenticated,)

    @detail_route(methods=['get'])
    def personas(self, request, pk=None):
        personas = Persona.objects.filter(**{self.attr: pk})
        serializer = PersonaSerializer(personas, many=True)
        return Response(serializer.data)


class CategoriaProfesionViewSet(BaseNameViewSet):
    queryset = CategoriaProfesion.objects.all()
    serializer_class = CategoriaProfesionSerializer
    attr = 'profesion_id'


class CategoriaServicioViewSet(BaseNameViewSet):
    queryset = CategoriaServicio.objects.all()
    serializer_class = CategoriaServicioSerializer
    attr = 'servicio_id'

