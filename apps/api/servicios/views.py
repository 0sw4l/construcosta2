from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.api.proveedores.views import BaseApiViewList
from apps.api.servicios.serializers import PersonaSerializer
from apps.servicios.models import Persona


class PersonaApiView(BaseApiViewList):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer

