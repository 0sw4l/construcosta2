from rest_framework import serializers

from apps.servicios.models import Persona, CategoriaServicio, CategoriaProfesion


class BaseSerializerName(serializers.ModelSerializer):
    class Meta:
        model = None
        fields = ('id', 'nombre', 'personas')


class CategoriaServicioSerializer(BaseSerializerName):
    class Meta(BaseSerializerName.Meta):
        model = CategoriaServicio


class CategoriaProfesionSerializer(BaseSerializerName):
    class Meta(BaseSerializerName.Meta):
        model = CategoriaProfesion


class PersonaSerializer(serializers.ModelSerializer):
    servicio_nombre = serializers.CharField(
        source='servicio.nombre',
        read_only=True
    )
    profesion_nombre = serializers.CharField(
        source='profesion.nombre',
        read_only=True
    )

    class Meta:
        model = Persona
        fields = ('username', 'get_full_name', 'servicio', 'servicio_nombre',
                  'profesion', 'profesion_nombre', 'get_absolute_url', 'foto')
