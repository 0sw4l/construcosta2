import json

from django.http import Http404
from django.http import HttpResponse
from rest_framework import generics
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics

from apps.api.ventas.serializers import PedidoServicioSerializer, PedidoServicioRankSerializer
from apps.utils.print_colors import _green, _blue, _orange
from apps.ventas.models import PedidoCliente, PedidoProductoItem, PedidoServicio


class VentaApiView(APIView):
    def post(self, request, format=None):
        proveedor_id = request.data['proveedor_id']
        cliente_id = request.data['cliente_id']
        print(_green(request.data))
        data = request.data.copy()
        if proveedor_id and cliente_id:
            venta = PedidoCliente.objects.create(proveedor_id=int(proveedor_id),
                                                 cliente_id=int(cliente_id))
            dictlist = []
            data.pop('cliente_id', None)
            data.pop('proveedor_id', None)
            for key, value in data.items():
                temp = [key, value]
                dictlist.append(temp)

            dictlist.sort()
            for el in dictlist:
                print(_blue(el))

            l = len(dictlist)

            for index, obj in enumerate(dictlist):
                if index < (l - 1):
                    if index == 0 or index % 2 == 0:
                        cantidad = dictlist[index][1]
                        id = dictlist[index + 1][1]
                        print('id : {1} , cantidad : {0}'.format(cantidad, id))
                        producto = PedidoProductoItem()
                        producto.pedido = venta
                        producto.producto_id = int(id)
                        producto.cantidad = int(cantidad)
                        producto.save()
                        print('creado : id : {1} , cantidad : {0}'.format(cantidad, id))
            return Response({
                'valor_venta': venta.get_total()
            }, status=status.HTTP_201_CREATED)
        else:
            print('nope')
        return Response(request.data, status=status.HTTP_201_CREATED)


class PedidoServicioApiView(APIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = PedidoServicioSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            total = serializer.data['total']
            return Response({
                'total': total
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def post(request):
    import json
    if request.POST:
        id = request.POST.get('id')
        calificacion = request.POST.get('calificacion')
        pedido = PedidoServicio.objects.get(id=int(id))
        print(calificacion)
        pedido.calificacion = calificacion
        pedido.save()
        return HttpResponse(
            json.dumps({'exito': 'pedido modificado'}),
            content_type="application/json"
        )
