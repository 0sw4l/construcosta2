from django.conf.urls import url, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^enviar-venta/', views.VentaApiView.as_view()),
    url(r'^enviar-pedido/', views.PedidoServicioApiView.as_view()),
    url(r'^calificar/', views.post),

]
