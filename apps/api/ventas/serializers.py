from rest_framework import serializers
from apps.ventas.models import PedidoServicio


class PedidoServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = PedidoServicio
        fields = ('id', 'cliente', 'total', 'persona', 'horas_servicio', 'fecha', 'hora', 'nota')


class PedidoServicioRankSerializer(serializers.ModelSerializer):
    class Meta:
        model = PedidoServicio
        fields = ('id', 'calificacion')
