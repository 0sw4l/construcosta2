from django.conf.urls import url, include
from rest_framework import routers

urlpatterns = [
    url(r'^proveedores/', include('apps.api.proveedores.urls')),
    url(r'^servicios/', include('apps.api.servicios.urls')),
    url(r'^ventas/', include('apps.api.ventas.urls')),
]
