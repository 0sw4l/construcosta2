from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import TemplateView

from apps.clientes.forms import ClienteForm
from apps.proveedores.forms import ProveedorForm
from apps.servicios.forms import PersonaForm


def log_in(request):
    context= {}
    context['error'] = False
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                if user.is_staff or user.is_superuser:
                    return redirect('admin:index')
                else:
                    return redirect('main:index')
            else:
                context['msj'] = 'El usuario ha sido desactivado'
                context['error'] = True
        else:
            context['msj'] = 'usuario o contraseña incorrecta'
            context['error'] = True
    context['form_cliente'] = ClienteForm()
    context['form_proveedor'] = ProveedorForm()
    context['form_persona'] = PersonaForm()
    return render(request, 'login.html', context)


@login_required
def salir(request):
    logout(request)
    return redirect('main:log_in')


class IndexTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'apps/main/start.html'


class CountTemplateView(TemplateView):
    template_name = 'apps/main/index.html'


def crear_usuario(request, **kwargs):
    template_name = 'form_registro.html'
    form = None
    if kwargs['user'] is 'cliente':
        form = ClienteForm
    elif kwargs['user'] is 'proveedor':
        form = ProveedorForm
    elif kwargs['user'] is 'persona':
        form = PersonaForm
    form_class = form(request.POST or None)
    if request.POST and form_class.is_valid():
        form_class.save()
        return redirect('main:log_in')
    return render(request, template_name, {'form': form_class})


