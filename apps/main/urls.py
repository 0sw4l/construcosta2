from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^inicio/', views.IndexTemplateView.as_view(), name='index'),
    url(r'^login/', views.log_in, name='log_in'),
    url(r'^registro-cliente/',
        views.crear_usuario,
        kwargs={'user': 'cliente'},
        name='crear_cliente'),
    url(r'^registro-proveedor/',
        views.crear_usuario,
        kwargs={'user': 'proveedor'},
        name='crear_proveedor'),
    url(r'^registro-persona/',
        views.crear_usuario,
        kwargs={'user': 'persona'},
        name='crear_persona'),
]
