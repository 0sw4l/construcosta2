from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'apps.main'

    def ready(self):
        from . import signals
        print('ready main')
