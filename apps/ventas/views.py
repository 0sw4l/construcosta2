from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.core import urlresolvers
# Create your views here.
from django.views.generic import DetailView
from django.views.generic import RedirectView

from apps.utils.views import BaseListViewDinamicHeader
from apps.ventas.models import PedidoCliente, PedidoProductoItem, PedidoServicio


class PedidoListViewMixin(BaseListViewDinamicHeader):
    model = PedidoCliente
    template_name = 'apps/ventas/lista_pedidos_cliente.html'


class PedidoClienteListView(PedidoListViewMixin):
    HEADER = ('proveedor', 'estado', 'fecha', 'valor')

    def get_queryset(self):
        filter = self.kwargs['filter']
        cliente = self.request.user.cliente
        kwargs = {'cliente': cliente}
        if filter is None:
            pass
        elif filter is 'enviados':
            kwargs['enviado'] = True
        elif filter is 'recibidos':
            kwargs['recibido'] = True
        return self.model.objects.filter(**kwargs).order_by('-fecha', '-hora')


class PedidoProveedorListView(PedidoListViewMixin):
    HEADER = ('cliente', 'estado', 'fecha', 'valor')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filtro'] = self.kwargs['filter']
        return context

    def get_queryset(self):
        filter = self.kwargs['filter']
        proveedor = self.request.user.proveedor
        kwargs = {'proveedor': proveedor}
        if filter is None:
            pass
        elif filter is 'enviados':
            kwargs['enviado'] = True
        elif filter is 'recibidos':
            kwargs['recibido'] = True
        return self.model.objects.filter(**kwargs).order_by('-fecha', '-hora')


class PedidoDetailView(LoginRequiredMixin, DetailView):
    model = PedidoCliente
    template_name = 'apps/ventas/detalle_pedido.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        productos = PedidoProductoItem.objects.filter(pedido_id=self.kwargs['pk'])
        print(productos)
        context['productos_pedido'] = productos
        return context


class SetPedidoView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        filter = self.kwargs['filter']
        pedido = PedidoCliente.objects.get(id=self.kwargs['pk'])
        if filter is 'enviado':
            pedido.enviado = True
        elif filter is 'recibido':
            pedido.recibido = True
        pedido.save()
        return urlresolvers.reverse_lazy('ventas:detalle_pedido',
                                         kwargs={
                                             'pk': self.kwargs['pk']
                                         })


class BasePedidoServicioListView(BaseListViewDinamicHeader):
    model = PedidoServicio
    template_name = 'apps/ventas/lista_pedidos_servicios_cliente.html'


class PedidoServicioClienteListView(BasePedidoServicioListView):
    HEADER = ('empleado', 'hora', 'fecha del servicio', 'fecha de solicitud', 'total')

    def get_queryset(self):
        return self.model.objects.filter(cliente=self.request.user.cliente)


class PedidoServicioPersonaListView(BasePedidoServicioListView):
    HEADER = ('cliente', 'persona', 'hora', 'fecha del servicio', 'fecha de solicitud')

    def get_queryset(self):
        return self.model.objects.filter(persona=self.request.user.persona)


class PedidoServicioDetailView(LoginRequiredMixin, DetailView):
    model = PedidoServicio
    template_name = 'apps/ventas/detalle_pedido_servicio.html'


class StartPedidoView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pedido = PedidoServicio.objects.get(id=self.kwargs['pk'])
        pedido.iniciado = True
        pedido.save()
        return urlresolvers.reverse_lazy('ventas:detalle_pedido_servicio',
                                         kwargs={
                                             'pk': self.kwargs['pk']
                                         })


class StopPedidoView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pedido = PedidoServicio.objects.get(id=self.kwargs['pk'])
        pedido.terminado = True
        pedido.iniciado = False
        pedido.save()
        return urlresolvers.reverse_lazy('ventas:detalle_pedido_servicio',
                                         kwargs={
                                             'pk': self.kwargs['pk']
                                         })
