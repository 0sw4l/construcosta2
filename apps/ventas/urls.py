from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^detalle-pedido/(?P<pk>\d+)/',
        views.PedidoDetailView.as_view(),
        name='detalle_pedido'),

    url(r'^pedidos/',
        views.PedidoProveedorListView.as_view(),
        kwargs={'filter': None},
        name='todos_los_pedidos'),

    url(r'^pedidos-enviados/',
        views.PedidoProveedorListView.as_view(),
        kwargs={'filter': 'enviados'},
        name='pedidos_enviados'),

    url(r'^pedidos-recibidos/',
        views.PedidoProveedorListView.as_view(),
        kwargs={'filter': 'recibidos'},
        name='pedidos_recibidos'),

    url(r'^pedidos-cliente/',
        views.PedidoClienteListView.as_view(),
        kwargs={'filter': None},
        name='todos_los_pedidos_cliente'),

    url(r'^pedidos-cliente-enviados/',
        views.PedidoClienteListView.as_view(),
        kwargs={'filter': 'enviados'},
        name='pedidos_enviados_cliente'),

    url(r'^pedidos-cliente-recibidos/',
        views.PedidoClienteListView.as_view(),
        kwargs={'filter': 'recibidos'},
        name='pedidos_recibidos_cliente'),

    url(r'^set-enviado/(?P<pk>\d+)/',
        views.SetPedidoView.as_view(),
        kwargs={'filter': 'enviado'},
        name='marcar_enviado'),

    url(r'^set-recibido/(?P<pk>\d+)/',
        views.SetPedidoView.as_view(),
        kwargs={'filter': 'recibido'},
        name='marcar_recibido'),

    url(r'^lista-servicios-cliente/',
        views.PedidoServicioClienteListView.as_view(),
        name='pedidos_servicio_cliente'),

    url(r'^lista-servicios-persona/',
        views.PedidoServicioPersonaListView.as_view(),
        name='pedidos_servicio_persona'),

    url(r'^pedido-servicio/(?P<pk>\d+)/',
        views.PedidoServicioDetailView.as_view(),
        name='detalle_pedido_servicio'),

    url(r'^iniciar-servicio/(?P<pk>\d+)/',
        views.StartPedidoView.as_view(),
        name='iniciar_servicio'),

    url(r'^terminar-servicio/(?P<pk>\d+)/',
        views.StopPedidoView.as_view(),
        name='terminar_servicio'),
]
