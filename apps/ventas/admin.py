from django.contrib import admin
from .models import PedidoCliente, PedidoProductoItem
# Register your models here.


@admin.register(PedidoCliente)
class PedidoClienteAdmin(admin.ModelAdmin):
    list_display = ['proveedor', 'cliente', 'get_total', 'get_estado']


@admin.register(PedidoProductoItem)
class PedidoProductoItem(admin.ModelAdmin):
    list_display = ['pedido', 'producto', 'cantidad', 'valor']
