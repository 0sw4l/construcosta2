from django.db import models
from django.core import urlresolvers
# Create your models here.


class PedidoCliente(models.Model):
    proveedor = models.ForeignKey('proveedores.Proveedor')
    cliente = models.ForeignKey('clientes.Cliente')
    enviado = models.BooleanField(default=False, editable=False)
    recibido = models.BooleanField(default=False, editable=False)
    nota = models.TextField(blank=True, null=True)
    hora = models.TimeField(auto_now_add=True)
    fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return 'pedido #{} creado {} , a las {}'.format(self.id, self.fecha, self.hora)

    def get_absolute_url(self):
        return urlresolvers.reverse_lazy('ventas:detalle_pedido',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def set_to_enviado(self):
        return urlresolvers.reverse_lazy('ventas:marcar_enviado',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def set_to_recibido(self):
        return urlresolvers.reverse_lazy('ventas:marcar_recibido',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def get_total(self):
        total = 0
        for item in PedidoProductoItem.objects.filter(pedido=self):
            total += item.valor
        return total

    def get_estado(self):
        estado = 'sin revisar'
        if self.recibido:
            estado = 'recibido'
        elif self.enviado:
            estado = 'enviado'
        return estado


class PedidoProductoItem(models.Model):
    pedido = models.ForeignKey(PedidoCliente, related_name='pedido_item')
    producto = models.ForeignKey('proveedores.ProductoProveedor')
    cantidad = models.PositiveIntegerField()
    valor = models.PositiveIntegerField(editable=False)

    def save(self, *args, **kwargs):
        self.valor = self.producto.valor * self.cantidad
        super().save(*args, **kwargs)


class PedidoServicio(models.Model):
    cliente = models.ForeignKey('clientes.Cliente')
    persona = models.ForeignKey('servicios.Persona')
    horas_servicio = models.PositiveIntegerField()
    total = models.PositiveIntegerField(editable=False)
    nota = models.TextField(blank=True, null=True)
    fecha = models.DateField()
    hora = models.TimeField()
    fecha_solicitud = models.DateField(auto_now_add=True)
    calificacion = models.PositiveIntegerField(default=0, editable=False)
    iniciado = models.BooleanField(default=False, editable=False)
    terminado = models.BooleanField(default=False, editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.total = self.persona.valor_hora * self.horas_servicio
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return urlresolvers.reverse_lazy('ventas:detalle_pedido_servicio',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def get_start_service_url(self):
        return urlresolvers.reverse_lazy('ventas:iniciar_servicio',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def get_stop_service_url(self):
        return urlresolvers.reverse_lazy('ventas:terminar_servicio',
                                         kwargs={
                                             'pk': self.pk
                                         })

    def get_rank_service_url(self):
        return urlresolvers.reverse_lazy('ventas:calificar_servicio',
                                             kwargs={
                                                 'pk': self.pk
                                             })

