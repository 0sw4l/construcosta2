from django.apps import AppConfig


class VentasConfig(AppConfig):
    name = 'apps.ventas'

    def ready(self):
        from . import signals
        print('ready ventas')
