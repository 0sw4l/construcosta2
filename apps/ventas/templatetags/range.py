from django import template as dj_template

register = dj_template.Library()


@register.filter(name='get_range')
def get_range(value):
    return range(value)

