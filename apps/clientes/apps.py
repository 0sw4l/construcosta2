from django.apps import AppConfig


class ClientesConfig(AppConfig):
    name = 'apps.clientes'

    def ready(self):
        from . import signals
        print('ready clientes')
