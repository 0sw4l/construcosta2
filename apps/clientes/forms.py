from apps.utils import forms
from . import models


class ClienteForm(forms.BaseUserCreationForm):
    title = 'Cliente'

    class Meta(forms.BaseUserCreationForm.Meta):
        model = models.Cliente

