from django.contrib import admin

# Register your models here.
from .forms import ClienteForm
from .models import Cliente


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ['get_full_name', 'nit', 'rut', 'direccion', 'telefono']
    form = ClienteForm



