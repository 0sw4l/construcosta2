from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from apps.pagos.models import TarjetaEmpresa
from apps.ventas.models import PedidoCliente, PedidoServicio


class Cliente(User):
    nit = models.CharField(max_length=50, unique=True, blank=True, null=True)
    rut = models.CharField(max_length=50, unique=True, blank=True, null=True)
    imagen = models.URLField(blank=True, null=True)
    direccion = models.CharField(max_length=50)
    telefono = models.PositiveIntegerField()
    TIPOS = (
        ('EMPRESA', 'EMPRESA'),
        ('PERSONA INDEPENDIENTE', 'PERSONA INDEPENDIENTE')
    )
    tipo = models.CharField(max_length=50, choices=TIPOS)

    class Meta:
        verbose_name = 'Usuario Cliente'
        verbose_name_plural = 'Usuarios Clientes'

    def __str__(self):
        return self.get_full_name().upper()

    def get_pedidos_servicio_all(self):
        return PedidoServicio.objects.filter(cliente=self)

    def get_pedidos_all(self):
        return PedidoCliente.objects.filter(cliente=self)

