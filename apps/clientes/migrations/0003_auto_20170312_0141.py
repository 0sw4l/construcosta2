# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-12 06:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0002_auto_20170311_0318'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cliente',
            old_name='tipo_cliente',
            new_name='tipo',
        ),
    ]
