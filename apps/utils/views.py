from django.contrib.auth import mixins
from django.views import generic
from django.views.generic import UpdateView
from apps.utils.shortcuts import get_object_or_none


class BaseListView(mixins.LoginRequiredMixin, generic.ListView):
    pass


class BaseCreateView(mixins.LoginRequiredMixin, generic.CreateView):
    template_name = 'forms/base.html'

    def get_context_data(self, **kwargs):
        context = super(BaseCreateView, self).get_context_data(**kwargs)
        context['action'] = 'Crear'
        return context


class BaseUpdateView(mixins.LoginRequiredMixin, UpdateView):
    template_name = 'forms/base.html'

    def get_context_data(self, **kwargs):
        context = super(BaseUpdateView, self).get_context_data(**kwargs)
        context['action'] = 'Modificar'
        return context

    def get_object(self, queryset=None):
        obj = self.model.objects.get(id=self.kwargs['pk'])
        return obj


class BaseCreateViewLocationPk(BaseCreateView):
    def get_form_kwargs(self):
        pk = self.kwargs['pk']
        kwargs = super().get_form_kwargs()
        kwargs['pk'] = pk
        kwargs['region'] = self.request.user.funcionario.sucursal.region
        return kwargs


class BaseListViewDinamicHeader(mixins.LoginRequiredMixin, generic.ListView):
    context_object_name = "list"
    paginate_by = 25
    query_fields = ()
    HEADER = None

    def __init__(self):
        super(BaseListViewDinamicHeader, self).__init__()
        self.HEADER += ('Acciones',)

    def get_queryset(self):
        return self.model.objects.all()

    def get_context_data(self, **kwargs):
        context = super(BaseListViewDinamicHeader, self).get_context_data(**kwargs)
        context['header_table'] = self.get_header_table()
        return context

    def get_header_table(self):
        return self.HEADER


class DirectDeleteMixin(object):
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class SetEmpresaFormView(generic.FormView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        user = self.request.user
        if hasattr(user, 'usuarioempresa'):
            kwargs['empresa_id'] = user.usuarioempresa.empresa.id


class SetProveedorFormView(generic.FormView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        user = self.request.user
        if hasattr(user, 'proveedor'):
            kwargs['proveedor_id'] = user.proveedor.id
        return kwargs


class FilterContentUserEmpresa(BaseListViewDinamicHeader):
    def get_queryset(self):
        return self.model.objects.filter(
            empresa=self.request.user.empres
        )


class FilterProveedorUser(BaseListViewDinamicHeader):
    def get_queryset(self):
        return self.model.objects.filter(
            proveedor=self.request.user.proveedor
        )

