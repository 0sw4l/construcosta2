from pipeline.compressors import CompressorBase
from csscompressor import compress


class CSSCompressor(CompressorBase):

    def compress_css(css):
        return compress(css)


class JSCompressor(CompressorBase):
    """
    JS compressor based on the Python library slimit
    (http://pypi.python.org/pypi/slimit/).
    """

    def compress_js(js):
        from slimit import minify
        return minify(js, mangle=True)
