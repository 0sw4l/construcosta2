from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from apps.clientes.models import Cliente
from apps.proveedores.models import Proveedor


class BaseUserCreationForm(UserCreationForm):
    title = None

    class Meta:
        model = User
        exclude = (
            'last_login',
            'is_staff',
            'is_superuser',
            'date_joined',
            'groups',
            'user_permissions',
            'password',
            'is_active',
            'email',
        )
        fields = '__all__'


class FormAllFields(forms.ModelForm):
    title = 'None'

    class Meta:
        model = None
        fields = '__all__'


class ClienteObjectMixinForm(forms.Form):
    cliente = forms.ModelChoiceField(
        queryset=Cliente.objects.all(),
        empty_label=None
    )

    def __init__(self, *args, **kwargs):
        id = kwargs.pop('cliente_id', None)
        super().__init__(*args, **kwargs)
        if id:
            queryset = Cliente.objects.filter(id=id)
            self.fields['empresa'].queryset = queryset


class ProveedorObjectMixinForm(forms.Form):
    proveedor = forms.ModelChoiceField(
        queryset=Proveedor.objects.all(),
        empty_label=None
    )

    def __init__(self, *args, **kwargs):
        id = kwargs.pop('proveedor_id', None)
        super().__init__(*args, **kwargs)
        if id:
            queryset = Proveedor.objects.filter(id=id)
            self.fields['proveedor'].queryset = queryset

