from .base import *

DEBUG = True

INSTALLED_APPS += ('gunicorn',)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

WSGI_APPLICATION = 'construcosta2.wsgi.production.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dfv4vb6tbcg4r6',
        'USER': 'ivspjhbpltxvld',
        'PASSWORD': '910b499592a82c54b0f60081cdcc548bdab932b97df254d0f4bad9d767a57528',
        'HOST': 'ec2-54-243-185-123.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}
