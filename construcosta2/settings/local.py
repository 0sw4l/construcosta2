from .base import *

DEBUG = True

WSGI_APPLICATION = 'construcosta2.wsgi.local.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

SERVER_DB = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dfv4vb6tbcg4r6',
        'USER': 'ivspjhbpltxvld',
        'PASSWORD': '910b499592a82c54b0f60081cdcc548bdab932b97df254d0f4bad9d767a57528',
        'HOST': 'ec2-54-243-185-123.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

LOCAL_DB = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../db.sqlite3'),
    }
}

DATABASES = LOCAL_DB


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [

]
