"""construcosta2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from apps.main import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.CountTemplateView.as_view(), name='count'),
    url(r'^salir/', views.salir, name='salir'),
    url(r'^main/', include('apps.main.urls', namespace='main')),
    url(r'^clientes/', include('apps.clientes.urls', namespace='clientes')),
    url(r'^pagos/', include('apps.pagos.urls', namespace='pagos')),
    url(r'^proveedores/', include('apps.proveedores.urls', namespace='proveedores')),
    url(r'^servicios/', include('apps.servicios.urls', namespace='servicios')),
    url(r'^ventas/', include('apps.ventas.urls', namespace='ventas')),
    url(r'^api/', include('apps.api.urls', namespace='api')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

