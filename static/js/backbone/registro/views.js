var app = app || {};
app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        'click .btn-form': 'hideModal',
        'click #cliente': 'showModalCliente',
        'click #proveedor': 'showModalProveedor',
        'click #persona': 'showModalPersona'
    },

    initialize: function () {
        console.log('estoy viva');
    },

    hideModal: function (e) {
        e.preventDefault();
        $('#registro').modal('hide');
        console.log('hide modal');
    },

    showModalCliente: function (e) {
        e.preventDefault();
        console.log('cliente');
        $('#label_cliente_proveedor').text('Registrarse como cliente/comprador');
        this.showModalClienteProveedor();
    },

    showModalProveedor: function (e) {
        e.preventDefault();
        console.log('proveedor');
        $('#label_cliente_proveedor').text('Registrarse como proveedor/vendedor');
        this.showModalClienteProveedor();

    },

    showModalPersona: function (e) {
        e.preventDefault();
        console.log('persona');
        $('#persona_modal').modal('show');
    },

    showModalClienteProveedor: function () {
        $('#cliente_proveedor_modal').modal('show');
    }





});

