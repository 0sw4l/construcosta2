var app = app || {};

var productos = Backbone.Collection.extend({
    model: app.ProductoModel,
    url: "/api/proveedores/productos/"
});

app.productosCollection = new productos();
