var app = app || {};
app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        'keyup #id_nombre': 'buscarProductoNombre',
        'change #id_marca': 'changeMarca',
        'change #id_categoria': 'changeCategoria'
    },

    initialize: function () {
        app.productosCollection.on('add', this.listarProductos);
        app.productosCollection.fetch();
    },

    changeMarca: function (ev) {
        window.marca = ev.target.value;
        app.productoCategoria = Backbone.Model.extend({
            urlRoot: '/api/proveedores/marcas/' + window.marca + '/productos/'
        });

        var productosCategoria = Backbone.Collection.extend({
            model: app.productoCategoria,
            url: '/api/proveedores/marcas/' + window.marca + '/productos/'
        });

        app.productoCategoriaCollection = new productosCategoria();

        console.log(window.marca);

        if (window.marca > 0) {
            console.log('filtro marcas');
            this.filtroCategoria();
        } else {
            window.marca = -1;
            this.agregarFiltro(app.productosCollection);
        }
    },

    changeCategoria: function (ev) {
        window.categoria = ev.target.value;
        app.productoCategoria = Backbone.Model.extend({
            urlRoot: '/api/proveedores/categorias/' + window.categoria + '/productos/'
        });

        var productosCategoria = Backbone.Collection.extend({
            model: app.productoCategoria,
            url: '/api/proveedores/categorias/' + window.categoria + '/productos/'
        });

        app.productoCategoriaCollection = new productosCategoria();

        console.log(window.categoria);

        if (window.categoria > 0) {
            console.log('filtro categorias');
            this.filtroCategoria();
        } else {
            window.categoria = -1;
            this.agregarFiltro(app.productosCollection);
        }
    },

     filtroCategoria: function () {
        var self = this;
        console.log('exito');
        app.productoCategoriaCollection.fetch({
            success: function (categorias) {
                console.log('success');
                self.agregarFiltro(categorias);
            }
        });
    },

    buscarProductoNombre: function () {
        var cadBuscador = $('#id_nombre').val().toLowerCase();
        var filtro = app.productosCollection.filter(function (modelo) {
            var cadModelo = modelo.get('nombre').substring(0, cadBuscador.length).toLowerCase();
            if ((cadBuscador === cadModelo) && (cadModelo.length == cadBuscador.length)
                && (cadBuscador.length > 0) && (cadModelo.length > 0)) {
                return modelo;
            } else if (cadBuscador.length == 0 && cadModelo.length == 0) {
                return modelo;
            }
        });
        this.agregarFiltro(filtro);
    },

    agregarFiltro: function (coleccionFiltro) {
        this.$('.lista-productos').html('');
        coleccionFiltro.forEach(this.listarProductos, this);
    },

    listarProductos: function (modelo) {
        var vista = new app.productoView({model: modelo});
        $('.lista-productos').append(vista.render().$el);
    }


});

app.productoView = Backbone.View.extend({
    template: _.template($('#productos_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

