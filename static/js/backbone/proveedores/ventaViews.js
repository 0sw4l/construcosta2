var app = app || {};

Humanize = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

app.productoVentaModel = Backbone.Model.extend({
    defaults: {
        cantidad: 0,
        producto_id: 0
    }
});

var productos_model = new app.productoVentaModel();

app.productosVentaCollection = Backbone.Collection.extend({
    model: app.productoVentaModel,
    url: '/api/pedidos/venta/'
});

app.ProductosCollection = new app.productosVentaCollection();


app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        'keyup #id_nombre': 'buscarProductoNombre',
        'click .add': 'addShop',
        'click .delete': 'deleteShop',
        'click #finish': 'sendShop'
    },

    initialize: function () {
        app.productoProveedorFilterId = Backbone.Model.extend({
            urlRoot: '/api/proveedores/productos-proveedor/' + window.proveedor + '/productos/'
        });

        var productosProveedor = Backbone.Collection.extend({
            model: app.productoProveedorFilterId,
            url: '/api/proveedores/productos-proveedor/' + window.proveedor + '/productos/'
        });

        app.productoProveedorFilterIdCollection = new productosProveedor();
        this.filtroProveedor();
    },


    filtroProveedor: function () {
        var self = this;
        console.log('exito');
        app.productoProveedorFilterIdCollection.fetch({
            success: function (productos) {
                console.log('success');
                self.agregarFiltro(productos);
            }
        });
    },

    sendShop: function () {
        if (app.ProductosCollection.length == 0)
            swal('Error',
                'no se puede hacer un pedido con 0 productos',
                'error');
        else {
            var productos_ = JSON.stringify(app.ProductosCollection.toJSON());
            productos_ = JSON.parse(productos_);
            productos_.forEach(function (v) {
                delete v.ganancia;
                delete v.nombre;
            });

            var jsonObject = {
                'productos': productos_,
                'proveedor_id': window.proveedor,
                'cliente_id': window.cliente
            };

            var self = this;

            $.ajax({
                url: '/api/ventas/enviar-venta/',
                type: 'POST',
                dataType: "json",
                data: jsonObject,
                success: function (data) {
                    if (data.error) {  // If there is an error, show the error messages
                        console.log("error : " + data.error);
                    } else {
                        swal('Pedido creado! ',
                            'el total de su pedido es $:' + Humanize(data.valor_venta),
                            'success');
                        self.resetUi(data.valor_venta);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    swal('Error', 'ha ocurrido un error', 'error');
                }
            });
        }
    },

    addShop: function (e) {
        e.preventDefault();
        var id = Number($(e.currentTarget).attr('id'));
        var producto = app.ProductosCollection.find({'producto_id': id});
        var valor_producto = Number($('#valor_' + id).text());
        if (producto) {
            var cantidad = producto.get('cantidad');
            producto.set('cantidad', cantidad + 1);
        } else {
            app.ProductosCollection.add([{
                cantidad: 1,
                producto_id: id,
                nombre: $('#name_' + id).text(),
                ganancia: valor_producto
            }]);

        }
        this.showGain();
        this.agregarProductoVenta();
    },

    deleteShop: function (e) {
        e.preventDefault();
        var id = Number($(e.currentTarget).attr('id'));
        var producto = app.ProductosCollection.find({'producto_id': id});
        app.ProductosCollection.remove(producto);
        this.showGain();
        this.agregarProductoVenta();
    },

    showGain: function () {
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#total_ganancia').animateNumber({
            number: this.getGain(),
            numberStep: comma_separator_number_step
        });
    },

    getGain: function () {
        var productos_ = JSON.stringify(app.ProductosCollection.toJSON());
        productos_ = JSON.parse(productos_);
        var gain = 0;
        productos_.forEach(function (v) {
            gain += v.cantidad * v.ganancia;
        });
        return gain
    },

    agregarProductoVenta: function () {
        this.$('.productos_venta').html('');
        app.ProductosCollection.forEach(this.agregarProductosVenta, this);
    },

    agregarProductosVenta: function (modelo) {
        var vista = new app.productoVentaView({model: modelo});
        $('.productos_venta').append(vista.render().$el);
    },

    buscarProductoNombre: function () {
        var cadBuscador = $('#id_nombre').val().toLowerCase();
        var filtro = app.productoProveedorFilterIdCollection.filter(function (modelo) {
            var cadModelo = modelo.get('nombre').substring(0, cadBuscador.length).toLowerCase();
            if ((cadBuscador === cadModelo) && (cadModelo.length == cadBuscador.length)
                && (cadBuscador.length > 0) && (cadModelo.length > 0)) {
                return modelo;
            } else if (cadBuscador.length == 0 && cadModelo.length == 0) {
                return modelo;
            }
        });
        this.agregarFiltro(filtro);
    },

    agregarFiltro: function (coleccionFiltro) {
        this.$('.lista-productos').html('');
        coleccionFiltro.forEach(this.listarProductos, this);
    },

    listarProductos: function (modelo) {
        var vista = new app.productoView({model: modelo});
        $('.lista-productos').append(vista.render().$el);
    },

     resetUi: function (ganancia) {
        this.$('.dom').html('');
        var vista = new app.SuccessView();
        $('#content_').append(vista.render().$el);
        $('#ganancia').append(Humanize(ganancia));
    }


});

app.productoView = Backbone.View.extend({
    template: _.template($('#productos_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});


app.productoVentaView = Backbone.View.extend({
    template: _.template($('#productos_venta_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

app.SuccessView = Backbone.View.extend({
    template: _.template($('#menu_after_success').html()),
    render: function () {
        console.log(this.ganancia);
        this.$el.html(this.template(this.ganancia));
        return this;
    }
});