var app = app || {};
var positions = [];
var positions_clon = [];
var _arrays = [];
var sequence = [];
var element_form = null;
app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        "click .days": "setTimeRow",
        "change #id_modelo": "getModelsetInitialPosition",
        "change #id_posicion_inicial": "getPositionModel",
        "click .vigilante-select": "showGetVigilanteModal",
        "click .btn-select": "assignGuard",
        "click .btn-unselect": "unassignGuard"
    },

    initialize: function () {
        this.lockOutLimitDays();

    },

    lockOutLimitDays: function () {
        var range = 3;
        for (form_set_prefix = 0; form_set_prefix < range; form_set_prefix++) {
            for (day = 1; day < window.limit_row; day++) {
                $("#servicio_item-" + form_set_prefix + "-dia_" + (day))
                    .addClass('hidden')
                    .parent()
                    .addClass('lock_row_item');
            }
        }

    },

    getModelsetInitialPosition: function (e) {
        $('#id_posicion_inicial').prop('selectedIndex', 0);
        this.resetProgramationTable();
        e.preventDefault();
        var model = $(e.currentTarget).val();
        if (model) {
            var url = '/api/operations/modelos-programacion/' + model;
            var position = $(e.currentTarget).val();
            var obj = $.ajax({
                type: "GET",
                url: url,
                async: false,
                success: function (data) {
                    positions = [];
                    for (var position in data)
                        positions.push(data[position]);
                }
            });
        }
    },

    getPositionModel: function (e) {
        e.preventDefault();
        var position = $(e.currentTarget).val();
        if ($("#id_modelo").val()) {
            if (position != undefined) {
                positions_clon = positions.set_index_position_from(position);
                this.setProgramationTable(positions_clon);
            }
        } else {
            swal("Error!",
                "Debe elegir un modelo de programacion para continuar!",
                "error")
        }

    },

    resetProgramationTable: function () {
        var range = 3;
        for (form_set_prefix = 0; form_set_prefix < range; form_set_prefix++) {
            for (day = window.limit_row; day <= window.day_last; day++) {
                $("#servicio_item-" + form_set_prefix + "-dia_" + day)
                    .text('x').addClass('days').removeClass('n').parent().removeClass('yellow_');
            }
        }
    },

    setProgramationTable: function (positions) {
        var range = 3;
        var start_ = 0;
        var $element_ = null;
        var msj = "";
        sequence = this.getAllSequences(positions);
        for (var form_set_prefix = 0; form_set_prefix < range; form_set_prefix++) {
            start_ = 0;
            for (var day = window.limit_row; day <= window.day_last; day++) {
                $element_ = $("#servicio_item-" + form_set_prefix + "-dia_" + day).addClass("n");
                $element_.text(sequence[form_set_prefix][start_]);
                $("#id_servicio_item-" + form_set_prefix + "-dia_" + day).val(sequence[form_set_prefix][start_]);
                if (sequence[form_set_prefix][start_] === "L")
                    $element_.parent().addClass('yellow_');
                else
                    $element_.parent().removeClass('yellow_');

                if (start_ != 5)
                    start_++;
                else
                    start_ = 0;

            }
            console.log(msj);
            msj = "";
        }

    },

    getAllSequences: function (array) {
        _arrays = [];
        for (var index = 0; index < array.length; index++) {
            if (index == 0 || index == 2 || index == 4) {
                _arrays.push(this.reorderSequence(array, index))
            }
        }
        console.log(_arrays);
        return _arrays;
    },

    reorderSequence: function (array, initial) {
        var index = initial;
        var clon = array;
        var part_1 = [];
        var part_2 = [];
        if (index > 0) {
            for (i = index; i < clon.length; i++)
                part_1.push(clon[i]);
            for (i = 0; i < index; i++)
                part_2.push(clon[i]);
            clon = part_1.concat(part_2);
        }
        console.log(clon);
        return clon;
    },

    setTimeRow: function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).attr("id");
        console.log(id);
        var mods = ["D", "N", "L", "x"];
        var $element_show = $("#" + id);
        var $select_hidden = $("#id_" + id);
        var $element_ = $element_show.text();

        switch ($element_show.text()) {
            case "x":
                $select_hidden.val("D");
                $element_show.text("D").addClass("n");
                break;
            case "D":
                $select_hidden.val("N");
                $element_show.text("N").addClass("n");
                break;
            case "N":
                $select_hidden.val("L");
                $element_show.text("L").addClass("n");
                $element_show.parent().addClass('yellow_');
                break;
            case "L":
                $select_hidden.val("x");
                $element_show.text("x").removeClass('n');
                $element_show.parent().removeClass('yellow_');
                break;
        }
    },

    showGetVigilanteModal: function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).attr("id");
        console.log(id);
        var guards_list = $('.list_guards');
        if (guards_list.html().length > 0) {
            guards_list.html('');
        }
        app.guardCollection = new app.VigilantesCollection();
        app.guardCollection.on('add', this.fillVigilanteList).fetch();
        $("#modal_vigilantes").modal("show");
        element_form = $("#"+id);
        console.log(element_form);
    },

    fillVigilanteList: function (model_) {
        var view = new app.VigilanteView({model: model_});
        $('.list_guards').append(view.render().$el);
    },

    assignGuard: function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).attr("data-user-id");
        element_form.text($("#"+id+"_name").text());
        console.log($("#id_"+element_form.attr('id')).val(id));
        $("#modal_vigilantes").modal("hide");
        this.setStatusGuard(Number(id), 1);
    },

    unassignGuard: function (e) {
        e.preventDefault();
        var id = $(e.currentTarget).attr("data-user-id");
        element_form.text($("#"+id+"_name").text());
        $("#modal_vigilantes").modal("hide");
        this.setStatusGuard(Number(id), 0);
    },


    setStatusGuard: function (id, operation) {
        var url = "/api/guards/vigilantes/"+id+"/set_status/";
        var request_object = {
            'id': id,
            'operation': operation
        };
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: request_object,
            success: function (data) {
                if (data.error) {  // If there is an error, show the error messages
                    console.log("error : " + data.error);
                } else {
                    $('.list_guards').html('');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                sweetAlert("Error...", "este producto ya esta en su stock!", "error");
                //console.clear();
            }
        });
    }

});


app.VigilanteView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#guards_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});
