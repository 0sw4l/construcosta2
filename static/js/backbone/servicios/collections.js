var app = app || {};

var personas = Backbone.Collection.extend({
    model: app.PersonaModel,
    url: '/api/servicios/personas/'
});

app.PersonasCollection = new personas();