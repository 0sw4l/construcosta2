var app = app || {};
app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        'keyup #id_nombre': 'buscarProductoNombre',
        'change #id_profesion': 'changeProfesion',
        'change #id_servicio': 'changeServicio'
    },

    initialize: function () {
        app.PersonasCollection.on('add', this.listarPersonas);
        app.PersonasCollection.fetch();
    },

    changeProfesion: function (ev) {
        window.profesion = ev.target.value;
        app.personaCategoriaModel = Backbone.Model.extend({
            urlRoot: '/api/servicios/categorias-profesion/' + window.profesion + '/personas/'
        });

        var personaCategoriaServicio = Backbone.Collection.extend({
            model: app.personaCategoriaModel,
            url:  '/api/servicios/categorias-profesion/' + window.profesion + '/personas/'
        });

        app.personaServicioCollection = new personaCategoriaServicio();

        console.log(window.profesion);

        if (window.profesion > 0) {
            console.log('filtro categorias');
            this.filtroCategoriaProfesion();
        } else {
            window.profesion = -1;
            this.agregarFiltro(app.PersonasCollection);
        }
    },

    changeServicio: function (ev) {
        window.servicio = ev.target.value;
        app.personaCategoriaModel = Backbone.Model.extend({
            urlRoot: '/api/servicios/categorias-servicios/' + window.servicio + '/personas/'
        });

        var personaCategoriaServicio = Backbone.Collection.extend({
            model: app.personaCategoriaModel,
            url:  '/api/servicios/categorias-servicios/' + window.servicio + '/personas/'
        });

        app.personaServicioCollection = new personaCategoriaServicio();

        console.log(window.servicio);

        if (window.servicio > 0) {
            console.log('filtro categorias');
            this.filtroCategoriaServicio();
        } else {
            window.servicio = -1;
            this.agregarFiltro(app.PersonasCollection);
        }
    },

     filtroCategoriaServicio: function () {
        var self = this;
        console.log('exito');
        app.personaServicioCollection.fetch({
            success: function (categorias) {
                console.log('success');
                self.agregarFiltro(categorias);
            }
        });
    },

    filtroCategoriaProfesion : function () {
        var self = this;
        console.log('exito');
        app.personaServicioCollection.fetch({
            success: function (categorias) {
                console.log('success');
                self.agregarFiltro(categorias);
            }
        });
    },

    buscarProductoNombre: function () {
        var cadBuscador = $('#id_nombre').val().toLowerCase();
        var filtro = app.productosCollection.filter(function (modelo) {
            var cadModelo = modelo.get('nombre').substring(0, cadBuscador.length).toLowerCase();
            if ((cadBuscador === cadModelo) && (cadModelo.length == cadBuscador.length)
                && (cadBuscador.length > 0) && (cadModelo.length > 0)) {
                return modelo;
            } else if (cadBuscador.length == 0 && cadModelo.length == 0) {
                return modelo;
            }
        });
        this.agregarFiltro(filtro);
    },

    agregarFiltro: function (coleccionFiltro) {
        this.$('.lista-personas').html('');
        coleccionFiltro.forEach(this.listarPersonas, this);
    },

    listarPersonas: function (modelo) {
        var vista = new app.personaView({model: modelo});
        $('.lista-personas').append(vista.render().$el);
    }


});

app.personaView = Backbone.View.extend({
    template: _.template($('#personas_item').html()),
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

