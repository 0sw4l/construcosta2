var app = app || {};

Humanize = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

var rank = 0;

app.mainView = Backbone.View.extend({
    el: "#app",

    events: {
        'click #enviar': 'validateData',
        'change #calificacion': 'setRank',
        'click #enviar_rank': 'sendRank'
    },

    initialize: function () {

    },

    setRank: function (e) {
        rank = e.target.value;

    },

    validateData: function (e) {
        e.preventDefault();
        var $horas = $('#horas').val();
        var $hora = $('#hora').val();
        var $fecha = $('#fecha').val();
        var $nota = $('#nota').val();

        if ($horas && $hora && $fecha){
            var object_request = {
                'horas_servicio': $horas,
                'hora': $hora,
                'fecha': $fecha,
                'persona': window.persona,
                'cliente': window.cliente
            };
            if($nota)
                object_request['nota'] = $nota;
            this.sendRequest(object_request);
        } else {
            swal("Error", "complete todos los camppos", "error");
        }

    },

    sendRequest: function (data) {
        var self = this;
            $.ajax({
                url: '/api/ventas/enviar-pedido/',
                type: 'POST',
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.error) {  // If there is an error, show the error messages
                        console.log("error : " + data.error);
                    } else {
                        swal('Servicio Solicitado! ',
                            'el total del servicio es :' + Humanize(data.total),
                            'success');
                        $('#contrato').modal('hide');
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    swal('Error', 'ha ocurrido un error', 'error');
                }
            });
    },

    sendRank: function () {
        var data = {
            'id': window.servicio,
            'calificacion': Number(rank)
        };
        var self = this;
            $.ajax({
                url: '/api/ventas/calificar/',
                type: 'POST',
                dataType: "json",
                data: data,
                success: function (data) {
                    if (data.error) {  // If there is an error, show the error messages
                        console.log("error : " + data.error);
                    } else {
                        location.reload();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    swal('Error', 'ha ocurrido un error', 'error');
                }
            });
    }



});
